import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-child-viewer',
  templateUrl: './child-viewer.component.html',
  styleUrls: ['./child-viewer.component.css']
})
export class ChildViewerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  childMessage = '';
  show : boolean = false;
  displayChildMessage(m: string) {
    this.childMessage = m;
    this.show = !this.show;
    console.log('displayChildMessage :: '+m);
  }

}

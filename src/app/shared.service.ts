import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor() { }

  private behaviorSubject = new BehaviorSubject('kings');
  behaviorObservable = this.behaviorSubject.asObservable();


  //set data into the behaviorSubject
  setServiceData(m: string) {
    this.behaviorSubject.next(m);
  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SharedService } from '../shared.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  angForm: FormGroup;
  constructor(private fb: FormBuilder, private sService: SharedService) {
    this.createForm();
  }


  registerVar = false;

  ngOnInit(): void {
    const reg = false;
  }

  createForm() {
    this.angForm = this.fb.group({
      userName: new FormControl('', Validators.required)
    });
  }

  onLogin() {    
    console.log('setting data into service :: ')
    //populate the service data once login button clicked
    this.sService.setServiceData('kings 11');
  }

  onClickRegister() {
    this.registerVar = true;
  }

}

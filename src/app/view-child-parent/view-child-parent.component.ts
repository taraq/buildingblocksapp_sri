import { Component, OnInit, ViewChild } from '@angular/core';
import { ViewChildChildComponent } from '../view-child-child/view-child-child.component';

@Component({
  selector: 'app-view-child-parent',
  templateUrl: './view-child-parent.component.html',
  styleUrls: ['./view-child-parent.component.css']
})
export class ViewChildParentComponent implements OnInit {

  //for child to parent data communication
  @ViewChild(ViewChildChildComponent) child: ViewChildChildComponent;

  constructor() { }

  childMessage: string = '';
  ngOnInit(): void { 
   }

  ngAfterViewInit() {
    console.log('inside ngAfterViewInit ' + this.child.viewChildMessage);
    this.childMessage = this.child.viewChildMessage;
  }
  
}

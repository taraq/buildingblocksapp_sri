import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.css']
})
export class ObservableComponent implements OnInit {

  constructor() { }

  obs:Observable<String>;
  ngOnInit(): void {
    this.obs = new Observable<String>(observer => {
      console.log('observer initialized ..');
      
      observer.next('observer message from Observable');
    });
  }
  // Consuming observable
  consumeObservable() {
    this.obs.subscribe(msg => {
      console.log('message from observable ....'+msg);
    }, err => {
      console.log('error in observable subscription..');
    });
    
  }

}

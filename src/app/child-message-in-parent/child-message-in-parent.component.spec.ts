import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildMessageInParentComponent } from './child-message-in-parent.component';

describe('ChildMessageInParentComponent', () => {
  let component: ChildMessageInParentComponent;
  let fixture: ComponentFixture<ChildMessageInParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildMessageInParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildMessageInParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

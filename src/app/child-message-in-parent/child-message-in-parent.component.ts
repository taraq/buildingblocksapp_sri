import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-child-message-in-parent',
  templateUrl: './child-message-in-parent.component.html',
  styleUrls: ['./child-message-in-parent.component.css']
})
export class ChildMessageInParentComponent implements OnInit {

  @Output() valueChange = new EventEmitter<String>();
  childMsg: string ;

  updateValue() {
    this.valueChange.emit('kings11');
  }
  constructor() { }

  ngOnInit(): void {
    this.valueChange.emit('kings12');
  }

}

import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';
import { ObservableComponent } from '../observable/observable.component';

// this component consumes shared-service
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  sService:any;
  messageFromService: string;
  constructor(sharedService: SharedService) {
    this.sService = sharedService;
  }

  ngOnInit(): void {
    
    this.sService.behaviorObservable.subscribe(behaviorSubject => {
      console.log('ngOnInIt :: message from service :: ' + behaviorSubject);
      this.messageFromService = behaviorSubject;
    });
  }

  obsComponent : ObservableComponent;
  // Consuming observable
  consumeObservable() {
    this.obsComponent.obs.subscribe(msg => {
      console.log('message from observable ....'+msg);
    });
  }


}

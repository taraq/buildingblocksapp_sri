import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BluejaytestComponent } from './bluejaytest.component';

describe('BluejaytestComponent', () => {
  let component: BluejaytestComponent;
  let fixture: ComponentFixture<BluejaytestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BluejaytestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BluejaytestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bluejaytest',
  // templateUrl: './bluejaytest.component.html',
  // styleUrls: ['./bluejaytest.component.css']
  template: `<input type="text" name="todo" [(ngModel)]="name"/>
              <button type="button" (click)="add()">Add</button><br/><br/>
              --{{name}}---`,
  styles: [`
    .is-done {
      text-decoration: line-through;
    }
  `]
})

export class BluejaytestComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public name: string = '';
  public items: Array<TodoItem> = [];

  public getRemainingCount() {
    return this.items.filter(item => !item.isDone).length;
  }

  public add() {
    let todo = new TodoItem();
    todo.name = this.name;
    todo.isDone = false;
    this.items.push(todo);
  }

  public toggleItem(item: TodoItem) {
    item.isDone = !item.isDone;
  }

}

class TodoItem {
  isDone: boolean;
  name: string;
  
}

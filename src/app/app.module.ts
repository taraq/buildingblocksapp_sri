import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ParentCompComponent } from './ParentMessageInChild/parent-comp.component';
import { ChildCompComponent } from './ParentMessageInChild/child-comp.component';

import { ViewChildParentComponent } from './view-child-parent/view-child-parent.component';
import { ViewChildChildComponent } from './view-child-child/view-child-child.component';

// for mat nav menu
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule} from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu'
import { FormsModule } from '@angular/forms';
import { ChildMessageInParentComponent } from './child-message-in-parent/child-message-in-parent.component';
import { ChildViewerComponent } from './child-viewer/child-viewer.component';
import { ObservableComponent } from './observable/observable.component';
import { AsyncAwaitComponent } from './async-await/async-await.component';
import { BluejaytestComponent } from './bluejaytest/bluejaytest.component';
import { Ej2r2lComponent } from './ejs2R2L/ej2r2l.component';

//for ej2r2l component
import { ListViewModule } from '@syncfusion/ej2-angular-lists';

//for ej2TreeLayout
import { DiagramModule, HierarchicalTreeService, DataBindingService } from '@syncfusion/ej2-angular-diagrams';
import { Ej2TreeLayoutComponent } from './ej2-tree-layout/ej2-tree-layout.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ParentCompComponent,
    ChildCompComponent,
    ViewChildParentComponent,
    ViewChildChildComponent,
    ChildMessageInParentComponent,
    ChildViewerComponent,
    ObservableComponent,
    AsyncAwaitComponent,
    BluejaytestComponent,
    Ej2r2lComponent,
    Ej2TreeLayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatIconModule, MatButtonModule, MatSidenavModule, MatToolbarModule, MatMenuModule, FormsModule,
    ListViewModule,
    DiagramModule
  ],
  providers: [HierarchicalTreeService, DataBindingService],
  bootstrap: [AppComponent]
})
export class AppModule { }

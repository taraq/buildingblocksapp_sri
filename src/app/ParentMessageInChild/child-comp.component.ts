import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'child-comp',
  templateUrl: './child-comp.component.html',
  styleUrls: ['./child-comp.component.css']
})
export class ChildCompComponent implements OnInit {

  //to show parent message in child component
  @Input() messageFromParent: string;

  constructor() {

  }

  ngOnInit(): void {
  }



}

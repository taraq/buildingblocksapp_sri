import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from './shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'BuildingBlocksProject';

  constructor(private router: Router, private sService: SharedService) {
    console.log("in app.component costructor");

    // router.events.subscribe((url) => {
    //     console.log("current url  :: ",url);
    // });
  }

  msg: string;
  date: Date = new Date();
  time;
  ngOnInit() {
    console.log('app.component : OnInIt :: ');
    this.sService.behaviorObservable.subscribe(m => this.msg = m);
    this.time = this.date.toTimeString();
  }

  onLogin() {
    alert('clikced on login button ::');
    this.router.navigate(['/login']);
  }

  manualRouting = function () {
    console.log('manual routing!');
  }

}

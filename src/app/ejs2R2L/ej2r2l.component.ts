import { Component, OnInit } from '@angular/core';
import { enableRtl } from '@syncfusion/ej2-base';

enableRtl(true);

@Component({
  selector: 'app-ej2r2l',
  templateUrl: './ej2r2l.component.html',
  styleUrls: ['./ej2r2l.component.css']
})
export class Ej2r2lComponent implements OnInit {
  ngOnInit(): void {
    // throw new Error('Method not implemented.');
  }

  public data: Object = [
    { class: 'facebook', socialMedia: 'Facebook', id: 'media1' },
    { class: 'twitter', socialMedia: 'Twitter', id: 'media2' },
    { class: 'tumblr', socialMedia: 'Tumblr', id: 'media4' },
    { class: 'google-plus', socialMedia: 'Google Plus', id: 'media5' },
    { class: 'skype', socialMedia: 'Skype', id: 'media6' },
    { class: 'vimeo', socialMedia: 'Vimeo', id: 'media7' },
    { class: 'instagram', socialMedia: 'Instagram', id: 'media8' },
    { class: 'youtube', socialMedia: 'YouTube', id: 'media9' },
  ];

  public fields: Object = { text: 'socialMedia' };
}
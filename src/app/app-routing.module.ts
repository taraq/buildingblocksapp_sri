import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import { HomeComponent } from './home/home.component';
//share data between components using @Input
import { ParentCompComponent } from './ParentMessageInChild/parent-comp.component';

//share data between components using @Output
import { ChildViewerComponent } from './child-viewer/child-viewer.component';

//share data between components using @ViewChild
import { ViewChildParentComponent } from './view-child-parent/view-child-parent.component';
import { ViewChildChildComponent } from './view-child-child/view-child-child.component';

import { ObservableComponent } from './observable/observable.component'; 
import { AsyncAwaitComponent } from './async-await/async-await.component';
import { BluejaytestComponent } from './bluejaytest/bluejaytest.component';
import { Ej2r2lComponent } from './ejs2R2L/ej2r2l.component';
import { Ej2TreeLayoutComponent } from './ej2-tree-layout/ej2-tree-layout.component';

const routes: Routes = [
  { path:'login', component:LoginComponent },
  { path:'home',component:HomeComponent },
  { path:'blujay',component:BluejaytestComponent },
  { path: 'parent-msg-in-child', component: ParentCompComponent },
  { path: 'child-msg-in-parent', component: ChildViewerComponent },
  { path: 'viewChildParent', component: ViewChildParentComponent},
  { path: 'viewChildChild', component: ViewChildChildComponent},
  { path: 'observables', component: ObservableComponent},
  { path: 'asyncAwait', component: AsyncAwaitComponent},
  { path: 'ej2sR2L', component: Ej2r2lComponent },
  { path: 'ej2TreeLayout', component: Ej2TreeLayoutComponent},
  { path: '', pathMatch:"full", redirectTo:'/login' },
  { path: '**', redirectTo: 'home'}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { 
  
}
